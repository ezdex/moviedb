﻿Para desarrollar la app fue necesario el complemento de todas las partes, en este caso fueron:

- Organización de proyecto en Trello
- Búsquedas de películas
- Diseño gráfico 
- Aportes multimedia
- Programación 


Para comenzar el proyecto se mantuvieron los mismos integrantes de la actividad anterior en Trello “organizar un asado””, César Soto, Micaela Escobedo, Lorena Gabriaguez y Jonathan Pazos. 
Jonathan comenzó creando el Trello y en paralelo un grupo de telegram para decidir las diferentes tareas y proponer ideas para la app.

- Búsquedas de películas: investigué las páginas wed en internet, realicé la búsqueda de los nombres de las películas, comparando y corroborando fechas de estrenos para armar los listados en Trello de las 4 diferentes sagas que contiene nuestra aplicación Movie DB. (Lorena Gabriaguez).

- Diseño gráfico: para la interfaz grafica de la aplicación, se utilizó imágenes descargada de internet, se editaron para poder agregarle el nombre de la aplicación (Movie DB), también se corrigió los colores de las imágenes para que quede acorde a la app, se utilizó la página Canva para esto. (Micaela Escobedo)

- Por mi lado (César Soto) obtengo la lista de sagas desde Trello y ubico los posters de las películas en la mejor calidad posible y los comparto para que otro integrante se ocupe de la parte gráfica, creo que la participación en este proyecto sumó a todos y todos participamos del mismo, con opiniones y diferentes aportes.

