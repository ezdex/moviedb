MOVIEDB es una aplicación que sirve para llevar un registro de las películas vistas o no vistas.

Dentro de la carpeta MovieDB se encuentran los archivos APK y .aia de la versión 1.0

Esta versión cuenta con 2 bases de datos ClouDB y TinyDB, se utilizó Cloudb para realizar la lista de catálogos y colocarlos en sagas.
Dentro de las sagas es posible ver las películas que la componen con una miniatura y al dar "click largo" estas se tildaran como vista o no vista.

Para el desarrollo de la mecánica se utilizó TinyDB para guardar cada una de las variables globales y así poder independizar cada portada con su respectivo resultado de tildes,
y al momento, tanto como volver a la pantalla anterior o cerrar y volver a abrir la APP que los resultados permanezcan en la misma manera que fueron dejados.
Gracias a este método es posible dejar registro de las películas que vallan viendo.

